## Walrange - A random numbers generator which state can be extracted and saved. It is proofed that the sequence is always the same with saving or without saving respectivly. 

This generator is useful for long time simulations when the computing must be interrupted and the state of the simulation must be saved. So it is neseccarily to save the state of the random number generator too.
With the saved and restored state of the simulation and the generator it is possible to proceed the simulation as there was no interruption and to gain reproducible results.

First there are two methods awailable: nextLong and nextInteger(range: Int).

## Usage

