
/**
  *
  * @author Falk Wallis
  *
  */

package de.wallisfalk.randomgenerators

import org.scalatest.{Matchers, PropSpec}
import de.wallisfalk.walrange.{PersistableConstantSequenceRandomGenerator => PersistableGenerator}
import org.scalatest.prop.TableDrivenPropertyChecks


class PersistableConstantRandomGeneratorPropSpec extends PropSpec  with TableDrivenPropertyChecks with Matchers {

   def testOneSet(seed: Int, seedEveryProcesses: Int, range: Int, testSequenceLength: Int, saveAtNum: Int) : Unit = {
      println("")
      println(s"Test with saveAtNum: $saveAtNum")

      val generator1 = PersistableGenerator(seed, seedEveryProcesses)
      val generator1AndInt = PersistableGenerator.nextInteger(generator1, range)
      val resultFirst = generator1AndInt._2
      var generator1AndIntegerRest = PersistableGenerator.nextInteger(generator1AndInt._1, range)
      val result1 = ((resultFirst + "-" + generator1AndInt._1.numberGenerations) +: (generator1AndIntegerRest._2 + "-" + generator1AndIntegerRest._1.numberGenerations) +: (2 until testSequenceLength).map(_ => {
         generator1AndIntegerRest = PersistableGenerator.nextInteger(generator1AndIntegerRest._1, range)
         generator1AndIntegerRest._2 + "-" + generator1AndIntegerRest._1.numberGenerations
      })).toList

      if (saveAtNum == 0) {
         val generator2 = PersistableGenerator(seed, seedEveryProcesses)
         // extract parameters and save it ...
         val parameters = generator2.extractParameters
         // load and restore generator ...
         val generator3 = PersistableGenerator.restore(parameters)
         val generator3AndInt = PersistableGenerator.nextInteger(generator3, range)
         val result3First = generator3AndInt._2
         var generator3AndIntegerRest = PersistableGenerator.nextInteger(generator3AndInt._1, range)
         val result3 = ((result3First + "-" + generator3AndInt._1.numberGenerations) +: (generator3AndIntegerRest._2 + "-" + generator3AndIntegerRest._1.numberGenerations) +: (2 until testSequenceLength).map(_ => {
            generator3AndIntegerRest = PersistableGenerator.nextInteger(generator3AndIntegerRest._1, range)
            generator3AndIntegerRest._2 + "-" + generator3AndIntegerRest._1.numberGenerations
         })).toList
         println("Test result1: " + result1)
         println("Test result3: " + result3)
         assert(result1 == result3)
      }
      else if (saveAtNum == 1) {
         val generator2 = PersistableGenerator(seed, seedEveryProcesses)
         val generator2AndInt = PersistableGenerator.nextInteger(generator2, range)
         val result2First = generator2AndInt._2

         // extract parameters and save it ...
         val parameters = generator2AndInt._1.extractParameters
         // load and restore generator ...
         val generator3 = PersistableGenerator.restore(parameters)
         var generator3AndIntegerRest = PersistableGenerator.nextInteger(generator3, range)
         val generator3AndIntegerRestFirst = generator3AndIntegerRest._2
         val result2 = ((generator2AndInt._2 + "-" + generator2AndInt._1.numberGenerations) +: (generator3AndIntegerRestFirst + "-" + generator3AndIntegerRest._1.numberGenerations) +: (2 until testSequenceLength).map(_ => {
            generator3AndIntegerRest = PersistableGenerator.nextInteger(generator3AndIntegerRest._1, range)
            generator3AndIntegerRest._2 + "-" + generator3AndIntegerRest._1.numberGenerations
         })).toList

         val result3 = result2

         println("Test result1: " + result1)
         println("Test result3: " + result3)

         assert(result1 == result3)
      }
      else {
         val generator2 = PersistableGenerator(seed, seedEveryProcesses)
         val generator2AndLong = PersistableGenerator.nextInteger(generator2, range)
         val result2First = generator2AndLong._2
         var generator2AndIntegerRest = PersistableGenerator.nextInteger(generator2AndLong._1, range)
         var result2: List[String] = List(s"$result2First-${generator2AndLong._1.numberGenerations}") ::: List(s"${generator2AndIntegerRest._2}-${generator2AndIntegerRest._1.numberGenerations}")
         println(result2)
         result2 = result2 ::: (2 until saveAtNum).map(_ => {
            generator2AndIntegerRest = PersistableGenerator.nextInteger(generator2AndIntegerRest._1, range)
            generator2AndIntegerRest._2.toString + "-" + generator2AndIntegerRest._1.numberGenerations.toString
         }).toList
         // extract parameters and save it ...
         val parameters = generator2AndIntegerRest._1.extractParameters
         // load and restore generator ...
         val generator3 = PersistableGenerator.restore(parameters)
         var generator3AndInteger = PersistableGenerator.nextInteger(generator3, range)
         val result3First = generator3AndInteger._2
         var result3 = result2 ::: List(result3First + "-" + generator3AndInteger._1.numberGenerations)
         result3 = result3 ::: (saveAtNum + 1 until testSequenceLength).map(_ => {
            generator3AndInteger = PersistableGenerator.nextInteger(generator3AndInteger._1, range)
            generator3AndInteger._2 + "-" + generator3AndInteger._1.numberGenerations
         }).toList
         println("Test result1: " + result1)
         println("Test result3: " + result3)

         assert(result1 == result3)
      }
   }

   val saveAtNums =
      Table(
         "saveAtNum",
         0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29
      )

   property("An interrupted and saved generator should proceed well") {
      forAll(saveAtNums) { saveAtNum =>
         testOneSet(100, 10, 100, 30, saveAtNum)
      }
   }

}
