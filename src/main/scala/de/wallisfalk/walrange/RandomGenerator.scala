
/**
  *
  * @author Falk Wallis
  *
  */

package de.wallisfalk.walrange

import scala.util.Random


/**
  * Case class 'RestoreParameters' used within methods 'extractParameters' and
  * 'restore' for saving purposes.
  *
  */
case class RestoreParameters(freshRandom: Boolean, lastSeed: Long, seedEveryGenerations: Int, currentGeneration: Int) {
  override def toString: String = s"RestoreParameters(freshRandom='$freshRandom', lastSeed='$lastSeed', seedEveryGenerations='$seedEveryGenerations', currentGeneration='$currentGeneration')"
}

/**
  * Sealed trait for a generalization of all generators.
  *
  */
sealed trait RandomGenerator

/**
  * Case class 'PersistableConstantSequenceRandomGenerator' and its companion object. This generator is designed for having
  * the ability to generate a predefined sequence of random outputs. The second ability is to save a state of the
  * generator and to restore it, to proceed the random sequence.
  *
  */
case class PersistableConstantSequenceRandomGenerator private(seed: Long, seedEveryNumberGenerations: Int, randomOption: Option[Random], numberGenerations: Int) extends RandomGenerator {

  def extractParameters: RestoreParameters = RestoreParameters(randomOption.isEmpty, this.seed, seedEveryNumberGenerations, numberGenerations)

  private def nextLongPrivate : (PersistableConstantSequenceRandomGenerator, Long) = {
    if (randomOption.isEmpty) {
      val random = new Random
      random.setSeed(seed)
      (PersistableConstantSequenceRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), random.nextLong())
    }
    else if (numberGenerations == seedEveryNumberGenerations)
      (PersistableConstantSequenceRandomGenerator(seed + 1, seedEveryNumberGenerations, None, 0), 0)._1.nextLongPrivate
    else
      (PersistableConstantSequenceRandomGenerator(seed, seedEveryNumberGenerations, randomOption, this.numberGenerations + 1), randomOption.get.nextLong())
  }

  private def nextIntegerPrivate(range: Int) : (PersistableConstantSequenceRandomGenerator, Int) = {
    require(range > 0, "The method 'nextInteger's parameter 'range' must be greater than zero!")

    if (randomOption.isEmpty) {
      val random = new Random
      random.setSeed(seed)
      (PersistableConstantSequenceRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), random.nextInt(range))
    }
    else if (numberGenerations == seedEveryNumberGenerations)
      (PersistableConstantSequenceRandomGenerator(seed + 1, seedEveryNumberGenerations, None, 0), 0)._1.nextIntegerPrivate(range)
    else
      (PersistableConstantSequenceRandomGenerator(seed, seedEveryNumberGenerations, randomOption, this.numberGenerations + 1), randomOption.get.nextInt(range))
  }
  require(seedEveryNumberGenerations >= 2, "The constructors parameter 'seedEveryNumberGenerations' must be equal or greater than two!")

  override def toString: String = s"PersistableConstantSequenceRandomGenerator(seed='$seed', seedEveryNumberGenerations='$seedEveryNumberGenerations', numberGenerations='$numberGenerations')"
}


case object PersistableConstantSequenceRandomGenerator {

  /**
    * The apply method for calling the private constructor.
    *
    * @param seed The seed of the internal scala.util.Random instance of type Long
    * @param seedEveryGenerations The number of generations when the internal scala.util.Random instance get a fresh
    *                           instance with a new seed (old seed incremented by one), it represents the maximum
    *                           number generations internally made and thrown away to get to the previous saved state.
    *
    */
  def apply(seed: Long, seedEveryGenerations: Int): PersistableConstantSequenceRandomGenerator = {
    PersistableConstantSequenceRandomGenerator(seed, seedEveryGenerations, None, 0)
  }


  def restore(restoreParameters: RestoreParameters): PersistableConstantSequenceRandomGenerator = {
    val generator = apply(restoreParameters.lastSeed, restoreParameters.seedEveryGenerations, None, 0)
    var generatorAndInteger = (generator, 0)
    if (restoreParameters.freshRandom)
      generator
    else {
      Range(1, restoreParameters.currentGeneration + 1).foreach(_ => {
        generatorAndInteger = nextInteger(generatorAndInteger._1)
      })
      generatorAndInteger._1
    }
  }

  def nextInteger(persistableGenerator: PersistableConstantSequenceRandomGenerator, range : Int = Int.MaxValue) :  (PersistableConstantSequenceRandomGenerator, Int) =
    persistableGenerator.nextIntegerPrivate(range)

  def nextLong(persistableGenerator: PersistableConstantSequenceRandomGenerator) : (PersistableConstantSequenceRandomGenerator, Long) =
    persistableGenerator.nextLongPrivate
}
